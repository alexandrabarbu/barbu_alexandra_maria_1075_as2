
const FIRST_NAME = "Alexandra Maria";
const LAST_NAME = "Barbu";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {};
    return {
        pageAccessCounter: (websiteSection = 'home') => 
        {
            websiteSection = websiteSection.toLowerCase();
            if (!cache[websiteSection]) 
                    cache[websiteSection] = 0;
            cache[websiteSection]++;
        },

        getCache: () => cache
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}
